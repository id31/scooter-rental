package com.senla.controller.exception;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.senla.service.impl.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;

@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(UsernameNotFoundException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse userNameNotFoundException(UsernameNotFoundException usernameNotFoundException) {
        return ErrorResponse.builder()
                .message(usernameNotFoundException.getMessage())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .date(LocalDate.now())
                .build();
    }

    @ExceptionHandler(AccessDeniedException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ErrorResponse accessDeniedException(AccessDeniedException accessDeniedException) {
        return ErrorResponse.builder()
                .message(accessDeniedException.getMessage())
                .httpStatus(HttpStatus.FORBIDDEN)
                .date(LocalDate.now())
                .build();
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse constraintViolationException(ConstraintViolationException constraintViolationException) {
        return ErrorResponse.builder()
                .message(constraintViolationException.getMessage())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .date(LocalDate.now())
                .build();
    }

    @ExceptionHandler(InvalidFormatException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse invalidFormatException(InvalidFormatException invalidFormatException) {
        return ErrorResponse.builder()
                .message(invalidFormatException.getMessage())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .date(LocalDate.now())
                .build();
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse badCredentialsException(BadCredentialsException badCredentialsException) {
        return ErrorResponse.builder()
                .message(badCredentialsException.getMessage())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .date(LocalDate.now())
                .build();
    }

    @ExceptionHandler(JwtAuthenticationException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse jwtAuthenticationException(JwtAuthenticationException jwtAuthenticationException) {
        return ErrorResponse.builder()
                .message(jwtAuthenticationException.getMessage())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .date(LocalDate.now())
                .build();
    }

    @ExceptionHandler(ServiceException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse serviceException(ServiceException serviceException) {
        return ErrorResponse.builder()
                .message(serviceException.getMessage())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                .date(LocalDate.now())
                .build();
    }
}
