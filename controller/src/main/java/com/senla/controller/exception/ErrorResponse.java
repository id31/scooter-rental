package com.senla.controller.exception;

import lombok.Builder;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@Data
public class ErrorResponse {

    String message;
    HttpStatus httpStatus;
    LocalDate date;
}
