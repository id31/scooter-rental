package com.senla.controller.controller;

import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.TariffCreateDto;
import com.senla.dto.dto.TariffReadDto;
import com.senla.entity.Tariff;
import com.senla.service.TariffService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import java.util.List;

@RestController
@RequestMapping("/tariffs")
public class TariffController {

    @Autowired
    private Validator validator;
    @Autowired
    private TariffService tariffService;

    @GetMapping
    public List<TariffReadDto> findAll() {
        return tariffService.findAll();
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        tariffService.delete(id);
        return ResponseEntity.ok("OK");
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity update(@PathVariable("id") Long id,
                                 @RequestBody TariffCreateDto tariffCreateDto) {
        InputValidator.validate(tariffCreateDto, validator);
        return ResponseEntity.ok(tariffService.update(id, tariffCreateDto));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_MANAGER"})
    public Tariff create(@RequestBody TariffCreateDto tariffCreateDto) {
        InputValidator.validate(tariffCreateDto, validator);
        return tariffService.create(tariffCreateDto);
    }
}
