package com.senla.controller.controller;

import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.dto.dto.OrderReadDto;
import com.senla.entity.Order;
import com.senla.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private Validator validator;
    @Autowired
    private OrderService orderService;

    @GetMapping
    @Secured({"ROLE_MANAGER"})
    public List<OrderReadDto> findAll() {
        return orderService.findAll();
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        orderService.delete(id);
        return ResponseEntity.ok("OK");
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_MANAGER", "ROLE_USER"})
    public ResponseEntity update(@PathVariable("id") Long id,
                                 @RequestBody OrderCreateDto orderCreateDto) {
        InputValidator.validate(orderCreateDto, validator);
        return ResponseEntity.ok(orderService.update(id, orderCreateDto));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_MANAGER", "ROLE_USER"})
    public Order create(@RequestBody OrderCreateDto orderCreateDto) {
        InputValidator.validate(orderCreateDto, validator);
        return orderService.create(orderCreateDto);
    }
}
