package com.senla.controller.controller;

import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.ScooterCreateDto;
import com.senla.dto.dto.ScooterReadDto;
import com.senla.entity.Scooter;
import com.senla.service.ScooterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import java.util.List;

@RestController
@RequestMapping("/scooters")
public class ScooterController {

    @Autowired
    private Validator validator;
    @Autowired
    private ScooterService scooterService;

    @GetMapping
    public List<ScooterReadDto> findAll() {
        return scooterService.findAll();
    }

    @GetMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity scootersDetails(@PathVariable("id") Long id) {
        return ResponseEntity.ok(scooterService.scootersDetails(id));
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity update(@PathVariable("id") Long id,
                                 @RequestBody ScooterCreateDto scooterCreateDto) {
        InputValidator.validate(scooterCreateDto, validator);
        return ResponseEntity.ok(scooterService.update(id, scooterCreateDto));
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        scooterService.delete(id);
        return ResponseEntity.ok("OK");
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_MANAGER"})
    public Scooter create(@RequestBody ScooterCreateDto scooterCreateDto) {
        InputValidator.validate(scooterCreateDto, validator);
        return scooterService.create(scooterCreateDto);
    }
}
