package com.senla.controller.controller;

import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.CustomerCreateDto;
import com.senla.dto.dto.CustomerReadDto;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.entity.Customer;
import com.senla.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private Validator validator;

    @GetMapping
    @Secured({"ROLE_MANAGER"})
    public List<CustomerReadDto> findAll() {
        return customerService.findAll();
    }

    @GetMapping("/history/{id}")
    @Secured({"ROLE_USER"})
    public ResponseEntity customersHistory(@PathVariable("id") Long id) {
        return ResponseEntity.ok(customerService.customersHistory(id));
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity update(@PathVariable("id") Long id,
                                 @RequestBody CustomerCreateDto customerCreateDto) {
        InputValidator.validate(customerCreateDto, validator);
        return ResponseEntity.ok(customerService.update(id, customerCreateDto));
    }

    @PutMapping("/close_order/{id}")
    @Secured({"ROLE_MANAGER", "ROLE_USER"})
    public ResponseEntity closeOrder(@PathVariable("id") Long id) {
        customerService.closeOrder(id);
        return ResponseEntity.ok("OK");
    }

    @PutMapping("/book")
    @Secured({"ROLE_MANAGER", "ROLE_USER"})
    public ResponseEntity bookScooter(@RequestBody OrderCreateDto orderCreateDto) {
        InputValidator.validate(orderCreateDto, validator);
        customerService.bookScooter(orderCreateDto);
        return ResponseEntity.ok("OK");
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        customerService.delete(id);
        return ResponseEntity.ok("OK");
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_MANAGER", "ROLE_USER"})
    public Customer create(@RequestBody CustomerCreateDto customerCreateDto) {
        InputValidator.validate(customerCreateDto, validator);
        return customerService.create(customerCreateDto);
    }
}
