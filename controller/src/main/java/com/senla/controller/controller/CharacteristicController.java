package com.senla.controller.controller;

import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.CharacteristicCreateDto;
import com.senla.dto.dto.CharacteristicReadDto;
import com.senla.entity.Characteristic;
import com.senla.service.CharacteristicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import java.util.List;

@RestController
@RequestMapping("/characteristics")
public class CharacteristicController {

    @Autowired
    private Validator validator;
    @Autowired
    private CharacteristicService characteristicService;

    @GetMapping
    public List<CharacteristicReadDto> findAll() {
        return characteristicService.findAll();
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        characteristicService.delete(id);
        return ResponseEntity.ok("OK");
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity update(@PathVariable("id") Long id,
                                 @RequestBody CharacteristicCreateDto characteristicCreateDto) {
        InputValidator.validate(characteristicCreateDto, validator);
        return ResponseEntity.ok(characteristicService.update(id, characteristicCreateDto));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_MANAGER"})
    public Characteristic create(@RequestBody CharacteristicCreateDto characteristicCreateDto) {
        InputValidator.validate(characteristicCreateDto, validator);
        return characteristicService.create(characteristicCreateDto);
    }
}
