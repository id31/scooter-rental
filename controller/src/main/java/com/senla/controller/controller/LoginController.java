package com.senla.controller.controller;

import com.senla.controller.security.JwtToken;
import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.UserCreateDto;
import com.senla.dto.dto.UserReadDto;
import com.senla.entity.User;
import com.senla.service.impl.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.Validator;

@RestController
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private UserService userService;
    @Autowired
    private JwtToken jwtToken;
    @Autowired
    private Validator validator;

    @PostMapping("/register")
    public String register(@RequestBody @Valid UserCreateDto userCreateDto) {
        InputValidator.validate(userCreateDto, validator);
        if (userService.findByLogin(userCreateDto.getLogin()) != null) {
            throw new UsernameNotFoundException("User with login" + userCreateDto.getLogin() + "exists");
        }
        userService.create(userCreateDto);
        return "OK";
    }

    @PostMapping("/auth")
    public ResponseEntity authentication(@RequestBody UserReadDto userReadDto) {
        try {
            String login = userReadDto.getLogin();
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(login, userReadDto.getPassword()));
            User user = userService.findByLogin(login);
            if (user == null) {
                throw new UsernameNotFoundException("User with login" + login + "nit found");
            }
            String token = jwtToken.generateToken(login, user.getRoles());
            return ResponseEntity.ok(token);
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid login or password");
        }
    }
}
