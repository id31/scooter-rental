package com.senla.controller.controller;

import com.senla.controller.validator.InputValidator;
import com.senla.dto.dto.RentalPointCreateDto;
import com.senla.dto.dto.RentalPointReadDto;
import com.senla.entity.RentalPoint;
import com.senla.service.RentalPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Validator;
import java.util.List;

@RestController
@RequestMapping("/rental_points")
public class RentalPointController {

    @Autowired
    private Validator validator;
    @Autowired
    private RentalPointService rentalPointService;

    @GetMapping
    public List<RentalPointReadDto> findAll() {
        return rentalPointService.findAll();
    }

    @GetMapping("/{id}")
    @Secured({"ROLE_MANAGER", "ROLE_USER"})
    public List showScooters(@PathVariable("id") Long id) {
        return rentalPointService.showScooters(id);
    }

    @DeleteMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity delete(@PathVariable("id") Long id) {
        rentalPointService.delete(id);
        return ResponseEntity.ok("OK");
    }

    @PutMapping("/{id}")
    @Secured({"ROLE_MANAGER"})
    public ResponseEntity update(@PathVariable("id") Long id,
                                 @RequestBody RentalPointCreateDto rentalPointCreateDto) {
        InputValidator.validate(rentalPointCreateDto, validator);
        return ResponseEntity.ok(rentalPointService.update(id, rentalPointCreateDto));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Secured({"ROLE_MANAGER"})
    public RentalPoint create(@RequestBody RentalPointCreateDto rentalPointCreateDto) {
        InputValidator.validate(rentalPointCreateDto, validator);
        return rentalPointService.create(rentalPointCreateDto);
    }
}
