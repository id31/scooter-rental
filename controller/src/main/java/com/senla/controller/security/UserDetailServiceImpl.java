package com.senla.controller.security;

import com.senla.entity.User;
import com.senla.service.impl.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@Slf4j
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Transactional
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = userService.findByLogin(login);
        if (user == null) {
            log.warn("User with login: {} not found", login);
            throw new UsernameNotFoundException("User with login" + login + "not found");
        }
        UserDetailsImpl userDetails = UserDetailsImpl.mapUserToUserDetails(user);
        log.info("User with login: {} successfully loaded", login);
        return userDetails;
    }
}
