package com.senla.entity;

import javax.persistence.*;
import java.time.LocalDate;

@javax.persistence.Entity
@Table(name = "orders")
public class Order extends Entity {

    @Column(name = "date_order")
    private LocalDate dateOrder;
    @Column(name = "date_refund")
    private LocalDate dateRefund;
    @OneToOne
    @JoinColumn(name = "tariff_id")
    private Tariff tariff;
    @Column(name = "quantity")
    private Integer quantityHours;
    @Column(name = "mileage")
    private Long mileage;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @ManyToOne()
    @JoinColumn(name = "scooter_id")
    private Scooter scooter;

    public Order() {
    }

    public Order(LocalDate dateOrder, LocalDate dateRefund, Long mileage, Tariff tariff,
                 Customer customer, Scooter scooter, Integer quantityHours) {
        this.dateOrder = dateOrder;
        this.dateRefund = dateRefund;
        this.mileage = mileage;
        this.tariff = tariff;
        this.customer = customer;
        this.scooter = scooter;
        this.quantityHours = quantityHours;
    }

    public Integer getQuantityHours() {
        return quantityHours;
    }

    public void setQuantityHours(Integer quantityMinute) {
        this.quantityHours = quantityMinute;
    }

    public LocalDate getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(LocalDate dateOrder) {
        this.dateOrder = dateOrder;
    }

    public LocalDate getDateRefund() {
        return dateRefund;
    }

    public void setDateRefund(LocalDate dateRefund) {
        this.dateRefund = dateRefund;
    }

    public Long getMileage() {
        return mileage;
    }

    public void setMileage(Long mileage) {
        this.mileage = mileage;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Scooter getScooter() {
        return scooter;
    }

    public void setScooter(Scooter scooter) {
        this.scooter = scooter;
    }

    @Override
    public String toString() {
        return "Order{" +
                "dateOrder=" + dateOrder +
                ", dateRefund=" + dateRefund +
                ", tariff=" + tariff +
                ", quantityHours=" + quantityHours +
                ", mileage=" + mileage +
                ", customer=" + customer +
                ", scooter=" + scooter +
                '}';
    }
}
