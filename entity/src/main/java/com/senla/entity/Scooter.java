package com.senla.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@javax.persistence.Entity
@Table(name = "scooters")
public class Scooter extends Entity {

    @Column(name = "number")
    private Integer number;
    @OneToOne
    @JoinColumn(name = "characteristic_id")
    private Characteristic characteristic;
    @Column(name = "scooter_state")
    @Enumerated(EnumType.STRING)
    private ScooterState scooterState;
    @JsonIgnore
    @OneToMany(mappedBy = "scooter", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Order> orders = new ArrayList<>();
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;
    @ManyToOne
    @JoinColumn(name = "rental_point_id")
    private RentalPoint rentalPoint;

    public Scooter() {
    }

    public Scooter(Integer number, Characteristic characteristic, ScooterState scooterState,
                   RentalPoint rentalPoint) {
        this.number = number;
        this.characteristic = characteristic;
        this.scooterState = scooterState;
        this.rentalPoint = rentalPoint;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Characteristic getCharacteristic() {
        return characteristic;
    }

    public void setCharacteristic(Characteristic characteristic) {
        this.characteristic = characteristic;
    }

    public ScooterState getScooterState() {
        return scooterState;
    }

    public void setScooterState(ScooterState scooterState) {
        this.scooterState = scooterState;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public RentalPoint getRentalPoint() {
        return rentalPoint;
    }

    public void setRentalPoint(RentalPoint rentalPoint) {
        this.rentalPoint = rentalPoint;
    }

    @Override
    public String toString() {
        return "Scooter{" +
                "number=" + number +
                ", characteristic=" + characteristic.getId() +
                ", scooterState=" + scooterState +
                ", orders=" + orders +
                ", customer=" + customer +
                ", rentalPoint=" + rentalPoint.getId() +
                '}';
    }
}
