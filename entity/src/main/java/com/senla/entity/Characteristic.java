package com.senla.entity;

import javax.persistence.Column;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "characteristics")
public class Characteristic extends Entity {

    @Column(name = "brand")
    private String brand;
    @Column(name = "model")
    private String model;
    @Column(name = "speed")
    private Long speed;
    @Column(name = "weight")
    private Integer weight;
    @Column(name = "size_wheels")
    private Integer sizeWheels;
    @Column(name = "power")
    private Long power;

    public Characteristic() {
    }

    public Characteristic(String brand, String model, Long speed, Integer weight, Integer sizeWheels, Long power) {
        this.brand = brand;
        this.model = model;
        this.speed = speed;
        this.weight = weight;
        this.sizeWheels = sizeWheels;
        this.power = power;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Long getSpeed() {
        return speed;
    }

    public void setSpeed(Long speed) {
        this.speed = speed;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getSizeWheels() {
        return sizeWheels;
    }

    public void setSizeWheels(Integer sizeWheels) {
        this.sizeWheels = sizeWheels;
    }

    public Long getPower() {
        return power;
    }

    public void setPower(Long power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Characteristic{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", speed=" + speed +
                ", weight=" + weight +
                ", sizeWheels=" + sizeWheels +
                ", power=" + power +
                '}';
    }
}
