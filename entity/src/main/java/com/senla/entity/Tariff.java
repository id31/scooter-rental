package com.senla.entity;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;

@javax.persistence.Entity
@Table(name = "tariffs")
public class Tariff extends Entity {

    @Column(name = "tariff_type")
    @Enumerated(EnumType.STRING)
    private TariffType tariffType;
    @Column(name = "price")
    private BigDecimal price;

    public Tariff() {
    }

    public Tariff(TariffType tariffType, BigDecimal price) {
        this.tariffType = tariffType;
        this.price = price;
    }

    public TariffType getTariffType() {
        return tariffType;
    }

    public void setTariffType(TariffType tariffType) {
        this.tariffType = tariffType;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "tariffType=" + tariffType +
                ", price=" + price +
                '}';
    }
}
