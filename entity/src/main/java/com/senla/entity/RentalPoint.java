package com.senla.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@javax.persistence.Entity
@Table(name = "rental_points")
public class RentalPoint extends Entity {

    @Column(name = "city")
    private String city;
    @Column(name = "street")
    private String street;
    @JsonIgnore
    @OneToMany(mappedBy = "rentalPoint", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Scooter> scooters = new ArrayList<>();

    public RentalPoint() {
    }

    public RentalPoint(String city, String street) {
        this.city = city;
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public List<Scooter> getScooters() {
        return scooters;
    }

    public void setScooters(List<Scooter> scooters) {
        this.scooters = scooters;
    }

    @Override
    public String toString() {
        return "RentalPoint{" +
                "city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", scooters=" + scooters +
                '}';
    }
}
