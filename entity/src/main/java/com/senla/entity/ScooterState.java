package com.senla.entity;

public enum ScooterState {

    REPAIRED, FREE, USED
}
