package com.senla.service.impl;

import com.senla.dao.repository.RoleRepository;
import com.senla.dao.repository.UserRepository;
import com.senla.dto.dto.UserCreateDto;
import com.senla.dto.mapper.UserCreateMapper;
import com.senla.entity.Role;
import com.senla.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserCreateMapper userCreateMapper;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Transactional
    public User create(UserCreateDto userCreateDto) {
        Role roleUser = roleRepository.findByName("ROLE_USER");
        List<Role> roles = new ArrayList<>();
        roles.add(roleUser);
        User user = userCreateMapper.map(userCreateDto);
        user.setRoles(roles);
        user.setPassword(encoder.encode(user.getPassword()));
        User userCreate = userRepository.create(user);
        log.info("Registered user: {} successfully", userCreate);
        return userCreate;
    }

    @Transactional
    public User findByLogin(String login) {
        User user = userRepository.findByLogin(login);
        if (user == null) {
            log.warn("User with login:{} doesn't exist", login);
            throw new ServiceException("User with such login doesn't exist");
        }
        log.info("User: {} found by login: {}", user, login);
        return user;
    }
}
