package com.senla.service.impl;

import com.senla.dao.repository.ScooterRepository;
import com.senla.dto.dto.ScooterCreateDto;
import com.senla.dto.dto.ScooterReadDto;
import com.senla.dto.mapper.ScooterCreateMapper;
import com.senla.dto.mapper.ScooterReadMapper;
import com.senla.entity.Scooter;
import com.senla.entity.ScooterState;
import com.senla.service.ScooterService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class ScooterServiceImpl implements ScooterService {

    @Autowired
    private ScooterRepository scooterRepository;
    @Autowired
    private ScooterReadMapper scooterReadMapper;
    @Autowired
    private ScooterCreateMapper scooterCreateMapper;

    @Transactional
    @Override
    public List<ScooterReadDto> findAll() {
        return scooterRepository.findAll()
                .stream()
                .map(scooterReadMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public boolean changeStatus(Long id, ScooterCreateDto scooterCreateDto) {
        return scooterRepository.changeStatus(id, scooterCreateDto.getScooterState());
    }

    @Transactional
    @Override
    public boolean changeCustomerId(Long id, Long customerId) {
        return scooterRepository.changeCustomerId(id, customerId);
    }

    @Transactional
    @Override
    public ScooterReadDto findById(Long id) {
        Scooter scooter = scooterRepository.findById(id);
        if (scooter == null) {
            log.warn("Scooter with id:{} doesn't exist", id);
            throw new ServiceException("Scooter with such id doesn't exist");
        }
        return scooterReadMapper.map(scooter);
    }

    @Transactional
    @Override
    public boolean update(Long id, ScooterCreateDto scooterCreateDto) {
        Integer number = scooterCreateDto.getNumber();
        Long characteristicId = scooterCreateDto.getCharacteristicId();
        ScooterState scooterState = scooterCreateDto.getScooterState();
        Long rentalPointId = scooterCreateDto.getRentalPointId();
        return scooterRepository.update(id, number, characteristicId, scooterState, rentalPointId);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Scooter scooter = scooterRepository.findById(id);
        if (scooter == null) {
            log.warn("Scooter with id:{} doesn't exist", id);
            throw new ServiceException("Scooter with such id doesn't exist");
        }
        scooterRepository.delete(scooter);
    }

    @Transactional
    @Override
    public Scooter create(ScooterCreateDto scooterCreateDto) {
        Scooter scooter = scooterCreateMapper.map(scooterCreateDto);
        return scooterRepository.create(scooter);
    }

    @Transactional
    @Override
    public List scootersDetails(Long id) {
        Scooter scooter = scooterRepository.findById(id);
        if (scooter == null) {
            log.warn("Scooter with id:{} doesn't exist", id);
            throw new ServiceException("Scooter with such id doesn't exist");
        }
        return scooterRepository.detailsScooter(id);
    }
}
