package com.senla.service.impl;

import com.senla.dao.repository.CustomerRepository;
import com.senla.dao.repository.OrderRepository;
import com.senla.dao.repository.ScooterRepository;
import com.senla.dto.dto.CustomerCreateDto;
import com.senla.dto.dto.CustomerReadDto;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.dto.mapper.CustomerCreateMapper;
import com.senla.dto.mapper.CustomerReadMapper;
import com.senla.dto.mapper.OrderCreateMapper;
import com.senla.entity.Customer;
import com.senla.entity.Order;
import com.senla.entity.Scooter;
import com.senla.entity.ScooterState;
import com.senla.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static com.senla.entity.ScooterState.FREE;
import static com.senla.entity.ScooterState.USED;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderCreateMapper orderCreateMapper;
    @Autowired
    private CustomerReadMapper customerReadMapper;
    @Autowired
    private ScooterRepository scooterRepository;
    @Autowired
    private CustomerCreateMapper customerCreateMapper;

    @Transactional
    @Override
    public List<CustomerReadDto> findAll() {
        return customerRepository.findAll()
                .stream()
                .map(customerReadMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public boolean update(Long id, CustomerCreateDto customerCreateDto) {
        String name = customerCreateDto.getName();
        String lastName = customerCreateDto.getLastName();
        String phoneNumber = customerCreateDto.getPhoneNumber();
        String email = customerCreateDto.getEmail();
        return customerRepository.updateCustomer(id, name, lastName, phoneNumber, email);
    }

    @Transactional
    @Override
    public CustomerReadDto findById(Long id) {
        Customer customer = customerRepository.findById(id);
        if (customer == null) {
            log.warn("Customer with id:{} doesn't exist", id);
            throw new ServiceException("Customer with such id doesn't exist");
        }
        return customerReadMapper.map(customer);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Customer customer = customerRepository.findById(id);
        if (customer == null) {
            log.warn("Customer with id:{} doesn't exist", id);
            throw new ServiceException("Customer with such id doesn't exist");
        }
        customerRepository.delete(customer);
    }

    @Transactional
    @Override
    public Customer create(CustomerCreateDto customerCreateDto) {
        Customer customer = customerCreateMapper.map(customerCreateDto);
        return customerRepository.create(customer);
    }

    @Transactional
    @Override
    public void bookScooter(OrderCreateDto orderCreateDto) {
        orderRepository.create(orderCreateMapper.map(orderCreateDto));
        Long customerId = orderCreateDto.getCustomerId();
        Long scooterId = orderCreateDto.getScooterId();
        Scooter scooter = scooterRepository.findById(scooterId);
        if (scooter.getScooterState().equals(USED)) {
            log.warn("Scooter with id:{} is being used", scooterId);
            throw new ServiceException("Scooter with such id is being used");
        }
        scooterRepository.changeCustomerId(orderCreateDto.getScooterId(), customerId);
        scooterRepository.changeStatus(orderCreateDto.getScooterId(), ScooterState.USED);
    }

    @Transactional
    @Override
    public void closeOrder(Long id) {
        Order order = orderRepository.findById(id);
        if (order == null) {
            log.warn("Order with id:{} doesn't exist", id);
            throw new ServiceException("Order with such id doesn't exist");
        }
        scooterRepository.changeStatus(order.getScooter().getId(), FREE);
        scooterRepository.changeCustomerId(order.getScooter().getId(), null);
    }

    @Transactional
    @Override
    public List customersHistory(Long id) {
        Customer customer = customerRepository.findById(id);
        if (customer == null) {
            log.warn("Customer with id:{} doesn't exist", id);
            throw new ServiceException("Customer with such id doesn't exist");
        }
        return customerRepository.customersHistory(id);
    }
}
