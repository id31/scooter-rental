package com.senla.service.impl;

import com.senla.dao.repository.CharacteristicRepository;
import com.senla.dto.dto.CharacteristicCreateDto;
import com.senla.dto.dto.CharacteristicReadDto;
import com.senla.dto.mapper.CharacteristicCreateMapper;
import com.senla.dto.mapper.CharacteristicReadMapper;
import com.senla.entity.Characteristic;
import com.senla.service.CharacteristicService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class CharacteristicServiceImpl implements CharacteristicService {

    @Autowired
    private CharacteristicRepository characteristicRepository;
    @Autowired
    private CharacteristicReadMapper characteristicReadMapper;
    @Autowired
    private CharacteristicCreateMapper characteristicCreateMapper;

    @Transactional
    @Override
    public List<CharacteristicReadDto> findAll() {
        return characteristicRepository.findAll()
                .stream()
                .map(characteristicReadMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public CharacteristicReadDto findById(Long id) {
        Characteristic characteristic = characteristicRepository.findById(id);
        if (characteristic == null) {
            log.warn("Characteristic with id:{} doesn't exist", id);
            throw new ServiceException("Characteristic with such id doesn't exist");
        }
        return characteristicReadMapper.map(characteristic);
    }

    @Transactional
    @Override
    public boolean update(Long id, CharacteristicCreateDto characteristicCreateDto) {
        String brand = characteristicCreateDto.getBrand();
        String model = characteristicCreateDto.getModel();
        Long power = characteristicCreateDto.getPower();
        Integer sizeWheels = characteristicCreateDto.getSizeWheels();
        Long speed = characteristicCreateDto.getSpeed();
        Integer weight = characteristicCreateDto.getWeight();
        return characteristicRepository.updateCharacteristic(id, brand, model,
                speed, power, weight, sizeWheels);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Characteristic characteristic = characteristicRepository.findById(id);
        if (characteristic == null) {
            log.warn("Characteristic with id:{} doesn't exist", id);
            throw new ServiceException("Characteristic with such id doesn't exist");
        }
        characteristicRepository.delete(characteristic);
    }

    @Transactional
    @Override
    public Characteristic create(CharacteristicCreateDto characteristicCreateDto) {
        Characteristic characteristic = characteristicCreateMapper.map(characteristicCreateDto);
        return characteristicRepository.create(characteristic);
    }
}
