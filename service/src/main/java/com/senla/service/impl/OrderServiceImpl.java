package com.senla.service.impl;

import com.senla.dao.repository.OrderRepository;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.dto.dto.OrderReadDto;
import com.senla.dto.mapper.OrderCreateMapper;
import com.senla.dto.mapper.OrderReadMapper;
import com.senla.entity.Order;
import com.senla.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderReadMapper orderReadMapper;
    @Autowired
    private OrderCreateMapper orderCreateMapper;

    @Transactional
    @Override
    public List<OrderReadDto> findAll() {
        return orderRepository.findAll()
                .stream()
                .map(orderReadMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public OrderReadDto findById(Long id) {
        Order order = orderRepository.findById(id);
        if (order == null) {
            log.warn("Order with id:{} doesn't exist", id);
            throw new ServiceException("Order with such id doesn't exist");
        }
        return orderReadMapper.map(order);
    }

    @Transactional
    @Override
    public boolean update(Long id, OrderCreateDto orderCreateDto) {
        LocalDate dateOrder = orderCreateDto.getDateOrder();
        LocalDate dateRefund = orderCreateDto.getDateRefund();
        Long customerId = orderCreateDto.getCustomerId();
        Integer quantityHours = orderCreateDto.getQuantityHours();
        Long scooterId = orderCreateDto.getScooterId();
        Long tariffId = orderCreateDto.getTariffId();
        return orderRepository.updateOrder(id, dateOrder, dateRefund,
                tariffId, quantityHours, customerId, scooterId);
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Order order = orderRepository.findById(id);
        if (order == null) {
            log.warn("Order with id:{} doesn't exist", id);
            throw new ServiceException("Order with such id doesn't exist");
        }
        orderRepository.delete(order);
    }

    @Transactional
    @Override
    public Order create(OrderCreateDto orderCreateDto) {
        Order order = orderCreateMapper.map(orderCreateDto);
        return orderRepository.create(order);
    }
}
