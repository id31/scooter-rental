package com.senla.service.impl;

import com.senla.dao.repository.TariffRepository;
import com.senla.dto.dto.TariffCreateDto;
import com.senla.dto.dto.TariffReadDto;
import com.senla.dto.mapper.TariffCreateMapper;
import com.senla.dto.mapper.TariffReadMapper;
import com.senla.entity.Tariff;
import com.senla.service.TariffService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TariffServiceImpl implements TariffService {

    @Autowired
    private TariffRepository tariffRepository;
    @Autowired
    private TariffReadMapper tariffReadMapper;
    @Autowired
    private TariffCreateMapper tariffCreateMapper;

    @Transactional
    @Override
    public List<TariffReadDto> findAll() {
        return tariffRepository.findAll()
                .stream()
                .map(tariffReadMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public TariffReadDto findById(Long id) {
        Tariff tariff = tariffRepository.findById(id);
        if (tariff == null) {
            log.warn("Tariff with id:{} doesn't exist", id);
            throw new ServiceException("Tariff with such id doesn't exist");
        }
        return tariffReadMapper.map(tariff);
    }

    @Transactional
    @Override
    public boolean update(Long id, TariffCreateDto tariffCreateDto) {
        return tariffRepository.updateTariff(id, tariffCreateDto.getTariffType(),
                tariffCreateDto.getPrice());
    }

    @Transactional
    @Override
    public void delete(Long id) {
        Tariff tariff = tariffRepository.findById(id);
        if (tariff == null) {
            log.warn("Tariff with id:{} doesn't exist", id);
            throw new ServiceException("Tariff with such id doesn't exist");
        }
        tariffRepository.delete(tariff);
    }

    @Transactional
    @Override
    public Tariff create(TariffCreateDto tariffCreateDto) {
        Tariff tariff = tariffCreateMapper.map(tariffCreateDto);
        return tariffRepository.create(tariff);
    }
}
