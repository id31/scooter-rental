package com.senla.service.impl;

import com.senla.dao.repository.RentalPointRepository;
import com.senla.dto.dto.RentalPointCreateDto;
import com.senla.dto.dto.RentalPointReadDto;
import com.senla.dto.mapper.RentalPointCreateMapper;
import com.senla.dto.mapper.RentalPointReadMapper;
import com.senla.entity.RentalPoint;
import com.senla.service.RentalPointService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RentalPointServiceImpl implements RentalPointService {

    @Autowired
    private RentalPointRepository rentalPointRepository;
    @Autowired
    private RentalPointReadMapper rentalPointReadMapper;
    @Autowired
    private RentalPointCreateMapper rentalPointCreateMapper;

    @Transactional
    @Override
    public List<RentalPointReadDto> findAll() {
        return rentalPointRepository.findAll()
                .stream()
                .map(rentalPointReadMapper::map)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public RentalPointReadDto findById(Long id) {
        RentalPoint rentalPoint = rentalPointRepository.findById(id);
        if (rentalPoint == null) {
            log.warn("Rental point with id:{} doesn't exist", id);
            throw new ServiceException("Rental point with such id doesn't exist");
        }
        return rentalPointReadMapper.map(rentalPoint);
    }

    @Transactional
    @Override
    public boolean update(Long id, RentalPointCreateDto rentalPointCreateDto) {
        return rentalPointRepository.updateRentalPoint(id, rentalPointCreateDto.getCity(),
                rentalPointCreateDto.getStreet());
    }

    @Transactional
    @Override
    public void delete(Long id) {
        RentalPoint rentalPoint = rentalPointRepository.findById(id);
        if (rentalPoint == null) {
            log.warn("Rental point with id:{} doesn't exist", id);
            throw new ServiceException("Rental point with such id doesn't exist");
        }
        rentalPointRepository.delete(rentalPoint);
    }

    @Transactional
    @Override
    public RentalPoint create(RentalPointCreateDto rentalPointCreateDto) {
        RentalPoint rentalPoint = rentalPointCreateMapper.map(rentalPointCreateDto);
        return rentalPointRepository.create(rentalPoint);
    }

    @Transactional
    @Override
    public List showScooters(Long id) {
        RentalPoint rentalPoint = rentalPointRepository.findById(id);
        if (rentalPoint == null) {
            log.warn("Rental point with id:{} doesn't exist", id);
            throw new ServiceException("Rental point with such id doesn't exist");
        }
        return rentalPointRepository.showRentalPointScooters(id);
    }
}
