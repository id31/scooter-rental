package com.senla.service;

import com.senla.dto.dto.CustomerCreateDto;
import com.senla.dto.dto.CustomerReadDto;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.entity.Customer;
import com.senla.entity.ScooterState;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CustomerService {

    List<CustomerReadDto> findAll();

    boolean update(Long id, CustomerCreateDto customerCreateDto);

    CustomerReadDto findById(Long id);

    void delete(Long id);

    Customer create(CustomerCreateDto customerCreateDto);

    void bookScooter(OrderCreateDto orderCreateDto);

    void closeOrder(Long id);

    List customersHistory(Long id);
}
