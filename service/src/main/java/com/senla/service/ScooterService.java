package com.senla.service;

import com.senla.dto.dto.ScooterCreateDto;
import com.senla.dto.dto.ScooterReadDto;
import com.senla.entity.Scooter;

import java.util.List;

public interface ScooterService {

    List<ScooterReadDto> findAll();

    boolean changeStatus(Long id, ScooterCreateDto scooterCreateDto);

    boolean changeCustomerId(Long id, Long customerId);

    ScooterReadDto findById(Long id);

    boolean update(Long id, ScooterCreateDto scooterCreateDto);

    void delete(Long id);

    Scooter create(ScooterCreateDto scooterCreateDto);

    List scootersDetails(Long id);
}
