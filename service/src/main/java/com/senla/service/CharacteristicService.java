package com.senla.service;

import com.senla.dto.dto.CharacteristicCreateDto;
import com.senla.dto.dto.CharacteristicReadDto;
import com.senla.entity.Characteristic;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CharacteristicService {

    List<CharacteristicReadDto> findAll();

    CharacteristicReadDto findById(Long id);

    @Transactional
    boolean update(Long id, CharacteristicCreateDto characteristicCreateDto);

    void delete(Long id);

    Characteristic create(CharacteristicCreateDto characteristicCreateDto);
}
