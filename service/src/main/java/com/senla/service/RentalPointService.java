package com.senla.service;

import com.senla.dto.dto.RentalPointCreateDto;
import com.senla.dto.dto.RentalPointReadDto;
import com.senla.entity.RentalPoint;

import java.util.List;

public interface RentalPointService {

    List<RentalPointReadDto> findAll();

    RentalPointReadDto findById(Long id);

    boolean update(Long id, RentalPointCreateDto rentalPointCreateDto);

    void delete(Long id);

    RentalPoint create(RentalPointCreateDto rentalPointCreateDto);

    List showScooters(Long id);
}
