package com.senla.service;

import com.senla.dto.dto.OrderCreateDto;
import com.senla.dto.dto.OrderReadDto;
import com.senla.entity.Order;

import java.util.List;

public interface OrderService {

    List<OrderReadDto> findAll();

    OrderReadDto findById(Long id);

    boolean update(Long id, OrderCreateDto orderCreateDto);

    void delete(Long id);

    Order create(OrderCreateDto orderCreateDto);
}
