package com.senla.service;

import com.senla.dto.dto.TariffCreateDto;
import com.senla.dto.dto.TariffReadDto;
import com.senla.entity.Tariff;

import java.util.List;

public interface TariffService {

    List<TariffReadDto> findAll();

    TariffReadDto findById(Long id);

    boolean update(Long id, TariffCreateDto tariffCreateDto);

    void delete(Long id);

    Tariff create(TariffCreateDto tariffCreateDto);
}
