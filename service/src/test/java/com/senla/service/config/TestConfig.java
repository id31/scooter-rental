package com.senla.service.config;

import com.senla.dao.repository.*;
import com.senla.dto.mapper.*;
import com.senla.service.*;
import com.senla.service.impl.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.mockito.Mockito.mock;

@Configuration
public class TestConfig {

    @Bean
    public CharacteristicService characteristicService() {
        return new CharacteristicServiceImpl();
    }

    @Bean
    public CustomerService customerService() {
        return new CustomerServiceImpl();
    }

    @Bean
    public OrderService orderService() {
        return new OrderServiceImpl();
    }

    @Bean
    public RentalPointService rentalPointService() {
        return new RentalPointServiceImpl();
    }

    @Bean
    public ScooterService scooterService() {
        return new ScooterServiceImpl();
    }

    @Bean
    public TariffService tariffService() {
        return new TariffServiceImpl();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public UserRepository userRepository() {
        return mock(UserRepository.class);
    }

    @Bean
    public RoleRepository roleRepository() {
        return mock(RoleRepository.class);
    }

    @Bean
    public RentalPointRepository rentalPointRepository() {
        return mock(RentalPointRepository.class);
    }

    @Bean
    public CustomerRepository customerRepository() {
        return mock(CustomerRepository.class);
    }

    @Bean
    public TariffRepository tariffRepository() {
        return mock(TariffRepository.class);
    }

    @Bean
    public ScooterRepository scooterRepository() {
        return mock(ScooterRepository.class);
    }

    @Bean
    public OrderRepository orderRepository() {
        return mock(OrderRepository.class);
    }

    @Bean
    public CharacteristicRepository characteristicRepository() {
        return mock(CharacteristicRepository.class);
    }

    @Bean
    public OrderCreateMapper orderCreateMapper() {
        return mock(OrderCreateMapper.class);
    }

    @Bean
    public UserCreateMapper userCreateMapper() {
        return mock(UserCreateMapper.class);
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public OrderReadMapper orderReadMapper() {
        return new OrderReadMapper();
    }

    @Bean
    public RentalPointCreateMapper rentalPointCreateMapper() {
        return mock(RentalPointCreateMapper.class);
    }

    @Bean
    public RentalPointReadMapper rentalPointReadMapper() {
        return new RentalPointReadMapper();
    }

    @Bean
    public CustomerReadMapper customerReadMapper() {
        return mock(CustomerReadMapper.class);
    }

    @Bean
    public CustomerCreateMapper customerCreateMapper() {
        return mock(CustomerCreateMapper.class);
    }

    @Bean
    public ScooterCreateMapper scooterCreateMapper() {
        return mock(ScooterCreateMapper.class);
    }

    @Bean
    public ScooterReadMapper scooterReadMapper() {
        return new ScooterReadMapper();
    }

    @Bean
    public CharacteristicReadMapper characteristicReadMapper() {
        return new CharacteristicReadMapper();
    }

    @Bean
    public TariffReadMapper tariffReadMapper() {
        return new TariffReadMapper();
    }

    @Bean
    public TariffCreateMapper tariffCreateMapper() {
        return mock(TariffCreateMapper.class);
    }

    @Bean
    public CharacteristicCreateMapper characteristicCreateMapper() {
        return mock(CharacteristicCreateMapper.class);
    }
}
