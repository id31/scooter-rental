package com.senla.service.impl;

import com.senla.dao.repository.TariffRepository;
import com.senla.dto.dto.TariffCreateDto;
import com.senla.dto.dto.TariffReadDto;
import com.senla.dto.mapper.TariffCreateMapper;
import com.senla.dto.mapper.TariffReadMapper;
import com.senla.entity.Tariff;
import com.senla.entity.TariffType;
import com.senla.service.TariffService;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class TariffServiceImplTest {

    @Autowired
    private TariffService tariffService;
    @Autowired
    private TariffRepository tariffRepository;
    @Autowired
    private TariffReadMapper tariffReadMapper;
    @Autowired
    private TariffCreateMapper tariffCreateMapper;
    private List<Tariff> mockedTariffs = mock(List.class);
    private List<TariffReadDto> mockedTariffReadDtos = mock(List.class);
    private Tariff tariff = mock(Tariff.class);
    private TariffReadDto tariffReadDto = mock(TariffReadDto.class);
    private TariffCreateDto tariffCreateDto = mock(TariffCreateDto.class);
    private Long id = 1L;

    @BeforeEach
    void init() {
        mockedTariffs = List.of(new Tariff(TariffType.HOURLY, new BigDecimal(4)),
                new Tariff(TariffType.SUBSCRIPTION, new BigDecimal(23)));
        mockedTariffReadDtos = mockedTariffs.stream().map(tariffReadMapper::map)
                .collect(Collectors.toList());
        tariff = new Tariff(TariffType.HOURLY, new BigDecimal(4));
        tariffReadDto = tariffReadMapper.map(tariff);
        tariffCreateDto = TariffCreateDto.builder()
                .tariffType(TariffType.HOURLY)
                .price(new BigDecimal(4))
                .build();
    }

    @Test
    void TariffServiceImpl_findAll() {
        doReturn(mockedTariffs).when(tariffRepository).findAll();

        List<TariffReadDto> expected = tariffService.findAll();
        List<TariffReadDto> actual = mockedTariffReadDtos;

        assertEquals(expected, actual);
    }

    @Test
    void TariffServiceImpl_findById() {
        doReturn(tariff).when(tariffRepository).findById(id);

        TariffReadDto expected = tariffService.findById(id);
        TariffReadDto actual = tariffReadDto;

        assertEquals(expected, actual);
    }

    @Test
    void TariffServiceImpl_update() {
        doReturn(true).when(tariffRepository).updateTariff(id, tariffCreateDto.getTariffType(),
                tariffCreateDto.getPrice());

        boolean update = tariffService.update(id, tariffCreateDto);

        assertTrue(update);
    }

    @Test
    void TariffServiceImpl_delete() {
        doReturn(null).when(tariffRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> tariffService.delete(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void TariffServiceImpl_create() {
        doReturn(tariff).when(tariffCreateMapper).map(tariffCreateDto);
        doReturn(tariff).when(tariffRepository).create(tariff);

        Tariff expected = tariffService.create(tariffCreateDto);
        Tariff actual = tariff;

        assertEquals(expected, actual);
    }
}