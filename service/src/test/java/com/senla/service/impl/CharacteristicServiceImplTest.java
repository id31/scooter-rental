package com.senla.service.impl;

import com.senla.dao.repository.CharacteristicRepository;
import com.senla.dto.dto.CharacteristicCreateDto;
import com.senla.dto.dto.CharacteristicReadDto;
import com.senla.dto.mapper.CharacteristicCreateMapper;
import com.senla.dto.mapper.CharacteristicReadMapper;
import com.senla.entity.Characteristic;
import com.senla.service.CharacteristicService;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class CharacteristicServiceImplTest {

    @Autowired
    private CharacteristicRepository characteristicRepository;
    @Autowired
    private CharacteristicCreateMapper characteristicCreateMapper;
    @Autowired
    private CharacteristicService characteristicService;
    @Autowired
    private CharacteristicReadMapper characteristicReadMapper;
    private Characteristic characteristic = mock(Characteristic.class);
    private CharacteristicReadDto characteristicReadDto = mock(CharacteristicReadDto.class);
    private CharacteristicCreateDto characteristicCreateDto = mock(CharacteristicCreateDto.class);
    private Long id;
    private List<Characteristic> mockedCharacteristic = mock(List.class);
    private List<CharacteristicReadDto> mockedCharacteristicReadDto = mock(List.class);

    @BeforeEach
    void init() {
        id = 1L;
        characteristic = new Characteristic(
                "Yedoo", "300SD", 14L, 5, 4, 150L);
        characteristicReadDto = characteristicReadMapper.map(characteristic);
        characteristicCreateDto = CharacteristicCreateDto.builder()
                .brand("Yedoo")
                .model("300Kl")
                .power(200L)
                .sizeWheels(2)
                .speed(15L)
                .weight(7)
                .build();
        mockedCharacteristic = List.of(
                new Characteristic("Yedoo", "300K", 20L, 7, 4, 200L),
                new Characteristic("SHULZ", "LB5600", 23L, 6, 4, 230L),
                new Characteristic("Micro", "Nano", 19L, 6, 5, 200L));
        mockedCharacteristicReadDto = mockedCharacteristic.stream().map(characteristicReadMapper::map)
                .collect(Collectors.toList());

    }

    @Test
    void CharacteristicServiceImpl_findAll() {
        when(characteristicRepository.findAll()).thenReturn(mockedCharacteristic);

        List<CharacteristicReadDto> expected = characteristicService.findAll();
        List<CharacteristicReadDto> actual = mockedCharacteristicReadDto;

        assertEquals(expected, actual);
    }

    @Test
    void CharacteristicServiceImpl_findById() {
        when(characteristicRepository.findById(id)).thenReturn(characteristic);

        CharacteristicReadDto expected = characteristicService.findById(id);

        assertEquals(expected, characteristicReadDto);
    }

    @Test
    void CharacteristicServiceImpl_update() {
        doReturn(true).when(characteristicRepository).updateCharacteristic(id,
                characteristicCreateDto.getBrand(), characteristicCreateDto.getModel(),
                characteristicCreateDto.getSpeed(), characteristicCreateDto.getPower(),
                characteristicCreateDto.getWeight(), characteristicCreateDto.getSizeWheels());

        boolean update = characteristicService.update(id, characteristicCreateDto);

        assertTrue(update);
    }

    @Test
    void CharacteristicServiceImpl_delete() {
        when(characteristicRepository.findById(id)).thenReturn(null);
        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> characteristicService.delete(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void CharacteristicServiceImpl_create() {
        doReturn(characteristic).when(characteristicCreateMapper).map(characteristicCreateDto);
        doReturn(characteristic).when(characteristicRepository).create(characteristic);

        Characteristic expected = characteristicService.create(characteristicCreateDto);

        assertEquals(expected, characteristic);
    }
}