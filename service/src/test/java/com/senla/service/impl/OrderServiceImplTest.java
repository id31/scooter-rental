package com.senla.service.impl;

import com.senla.dao.repository.OrderRepository;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.dto.dto.OrderReadDto;
import com.senla.dto.mapper.OrderCreateMapper;
import com.senla.dto.mapper.OrderReadMapper;
import com.senla.entity.*;
import com.senla.service.OrderService;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class OrderServiceImplTest {

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderReadMapper orderReadMapper;
    @Autowired
    private OrderCreateMapper orderCreateMapper;
    @Autowired
    private OrderRepository orderRepository;
    private List<Order> mockedOrders = mock(List.class);
    private List<OrderReadDto> mockedReadDtos = mock(List.class);
    private Tariff tariff = mock(Tariff.class);
    private Customer customer = mock(Customer.class);
    private Scooter scooter = mock(Scooter.class);
    private Order order = mock(Order.class);
    private OrderReadDto orderReadDto = mock(OrderReadDto.class);
    private OrderCreateDto orderCreateDto = mock(OrderCreateDto.class);
    private Long id = 1L;

    @BeforeEach
    void init() {
        tariff = new Tariff(TariffType.HOURLY, new BigDecimal(4));
        customer = new Customer("Petr", "Petrov",
                "+375333121222", "testpetr@mail.ru");
        scooter = new Scooter(100,
                new Characteristic("Yedoo", "300LK", 12L, 4, 4, 200L),
                ScooterState.FREE,
                new RentalPoint("Grodno", "Lenina"));
        order = new Order(LocalDate.of(2022, 4, 12),
                LocalDate.of(2022, 4, 12), 12L,
                tariff, customer, scooter, 2);
        orderReadDto = orderReadMapper.map(order);
        orderCreateDto = OrderCreateDto.builder()
                .dateOrder(LocalDate.of(2022, 4, 12))
                .dateRefund(LocalDate.of(2022, 4, 12))
                .tariffId(id)
                .scooterId(id)
                .mileage(12L)
                .quantityHours(2)
                .customerId(id)
                .build();
        mockedOrders = List.of(new Order(LocalDate.of(2022, 4, 12),
                        LocalDate.of(2022, 4, 12), 12L,
                        tariff, customer, scooter, 2),
                new Order(LocalDate.of(2022, 5, 14),
                        LocalDate.of(2022, 5, 14), 15L,
                        tariff, customer, scooter, 3));
        mockedReadDtos = mockedOrders.stream().map(orderReadMapper::map)
                .collect(Collectors.toList());
    }

    @Test
    void OrderServiceImpl_findAll() {
        doReturn(mockedOrders).when(orderRepository).findAll();

        List<OrderReadDto> expected = orderService.findAll();
        List<OrderReadDto> actual = mockedReadDtos;

        assertEquals(expected, actual);
    }

    @Test
    void OrderServiceImpl_findById() {
        doReturn(order).when(orderRepository).findById(id);

        OrderReadDto expected = orderService.findById(id);
        OrderReadDto actual = orderReadDto;

        assertEquals(expected, actual);
    }

    @Test
    void OrderServiceImpl_update() {
        doReturn(true).when(orderRepository).updateOrder(id, orderCreateDto.getDateOrder(),
                orderCreateDto.getDateRefund(), orderCreateDto.getTariffId(),
                orderCreateDto.getQuantityHours(), orderCreateDto.getCustomerId(),
                orderCreateDto.getScooterId());

        boolean expected = orderService.update(id, orderCreateDto);

        assertTrue(expected);
    }

    @Test
    void OrderServiceImpl_delete() {
        doReturn(null).when(orderRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> orderService.delete(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void OrderServiceImpl_create() {
        doReturn(order).when(orderCreateMapper).map(orderCreateDto);
        doReturn(order).when(orderRepository).create(order);

        Order expected = orderService.create(orderCreateDto);
        Order actual = order;

        assertEquals(expected, actual);
    }
}