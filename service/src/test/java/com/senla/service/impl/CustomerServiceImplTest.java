package com.senla.service.impl;

import com.senla.dao.repository.CustomerRepository;
import com.senla.dao.repository.OrderRepository;
import com.senla.dao.repository.ScooterRepository;
import com.senla.dto.dto.CustomerCreateDto;
import com.senla.dto.dto.CustomerReadDto;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.dto.mapper.CustomerCreateMapper;
import com.senla.dto.mapper.CustomerReadMapper;
import com.senla.entity.*;
import com.senla.service.CustomerService;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class CustomerServiceImplTest {

    @Autowired
    private CustomerService customerService;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private ScooterRepository scooterRepository;
    @Autowired
    private CustomerReadMapper customerReadMapper;
    @Autowired
    private CustomerCreateMapper customerCreateMapper;
    private Long id = 1L;
    private Customer customer = mock(Customer.class);
    private Scooter scooter = mock(Scooter.class);
    private CustomerReadDto customerReadDto = mock(CustomerReadDto.class);
    private CustomerCreateDto customerCreateDto = mock(CustomerCreateDto.class);
    private OrderCreateDto orderCreateDto = mock(OrderCreateDto.class);
    private List<Customer> mockCustomers = mock(List.class);
    private List<CustomerReadDto> mockCustomersReadDto = mock(List.class);

    @BeforeEach
    void init() {
        customer = new Customer("Petr", "Petrov",
                "+3753331234567", "testpetr@mail.ru");
        customerReadDto = customerReadMapper.map(customer);
        scooter = new Scooter(100, new Characteristic("Yedoo", "300LK", 12L, 4, 4, 200L),
                ScooterState.USED, new RentalPoint("Grodno", "Lenina"));
        orderCreateDto = OrderCreateDto.builder()
                .customerId(id)
                .dateOrder(LocalDate.of(2022, 4, 12))
                .dateRefund(LocalDate.of(2022, 4, 12))
                .mileage(12L)
                .quantityHours(2)
                .scooterId(id)
                .tariffId(id)
                .build();
        mockCustomers = List.of(
                new Customer("Ivan", "Ivanov",
                        "+375294567899", "ivanivan@gmail.com"),
                new Customer("Petr", "Petrov",
                        "+375331234567", "testpetr@mail.ru"),
                new Customer("Sergey", "Cripov",
                        "+375253451199", "ppris@inbox.ru"));
        mockCustomersReadDto = mockCustomers.stream().map(customerReadMapper::map)
                .collect(Collectors.toList());
        customerCreateDto = CustomerCreateDto.builder()
                .name("Petr")
                .lastName("Petrov")
                .phoneNumber("+375331234567")
                .email("testpetr@mail.ru")
                .build();
    }

    @Test
    void CustomerServiceImpl_findAll() {
        when(customerRepository.findAll()).thenReturn(mockCustomers);

        List<CustomerReadDto> expected = customerService.findAll();
        List<CustomerReadDto> actual = mockCustomersReadDto;

        assertEquals(expected, actual);
    }

    @Test
    void CustomerServiceImpl_update() {
        doReturn(true).when(customerRepository).updateCustomer(id, customerCreateDto.getName(),
                customerCreateDto.getLastName(), customerCreateDto.getPhoneNumber(), customerCreateDto.getEmail());

        boolean update = customerService.update(id, customerCreateDto);

        assertTrue(update);
    }

    @Test
    void CustomerServiceImpl_findById() {
        doReturn(customer).when(customerRepository).findById(id);

        CustomerReadDto expected = customerService.findById(id);

        assertEquals(expected, customerReadDto);
    }

    @Test
    void CustomerServiceImpl_delete() {
        doReturn(null).when(customerRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class, () -> customerService.delete(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void CustomerServiceImpl_create() {
        doReturn(customer).when(customerCreateMapper).map(customerCreateDto);
        doReturn(customer).when(customerRepository).create(customer);

        Customer expected = customerService.create(customerCreateDto);

        assertEquals(expected, customer);
    }

    @Test
    void CustomerServiceImpl_bookScooter() {
        doReturn(scooter).when(scooterRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> customerService.bookScooter(orderCreateDto));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void CustomerServiceImpl_closeOrder() {
        doReturn(null).when(orderRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> customerService.closeOrder(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void CustomerServiceImpl_customersHistory() {
        doReturn(null).when(customerRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> customerService.customersHistory(id));

        assertNotNull(serviceException.getMessage());
    }
}