package com.senla.service.impl;

import com.senla.dao.repository.ScooterRepository;
import com.senla.dto.dto.ScooterCreateDto;
import com.senla.dto.dto.ScooterReadDto;
import com.senla.dto.mapper.ScooterCreateMapper;
import com.senla.dto.mapper.ScooterReadMapper;
import com.senla.entity.Characteristic;
import com.senla.entity.RentalPoint;
import com.senla.entity.Scooter;
import com.senla.entity.ScooterState;
import com.senla.service.ScooterService;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class ScooterServiceImplTest {

    @Autowired
    private ScooterService scooterService;
    @Autowired
    private ScooterRepository scooterRepository;
    @Autowired
    private ScooterReadMapper scooterReadMapper;
    @Autowired
    private ScooterCreateMapper scooterCreateMapper;
    private List<Scooter> mockedScooters = mock(List.class);
    private List<ScooterReadDto> mockedScooterReadDtos = mock(List.class);
    private Scooter scooter = mock(Scooter.class);
    private ScooterReadDto scooterReadDto = mock(ScooterReadDto.class);
    private ScooterCreateDto scooterCreateDto = mock(ScooterCreateDto.class);
    private Long id = 1L;

    @BeforeEach
    void init() {
        mockedScooters = List.of(
                new Scooter(100,
                        new Characteristic("Yedoo", "300LK",
                                12L, 4, 4, 200L),
                        ScooterState.FREE,
                        new RentalPoint("Grodno", "Lenina")),
                new Scooter(102,
                        new Characteristic("SHULZ", "PK800",
                                14L, 5, 4, 250L),
                        ScooterState.FREE,
                        new RentalPoint("Grodno", "Gorky")));
        mockedScooterReadDtos = mockedScooters.stream().map(scooterReadMapper::map)
                .collect(Collectors.toList());
        scooter = new Scooter(100,
                new Characteristic("Yedoo", "300LK",
                        12L, 4, 4, 200L),
                ScooterState.FREE,
                new RentalPoint("Grodno", "Lenina"));
        scooterReadDto = scooterReadMapper.map(scooter);
        scooterCreateDto = ScooterCreateDto.builder()
                .characteristicId(id)
                .number(100)
                .scooterState(ScooterState.FREE)
                .rentalPointId(id)
                .build();
    }

    @Test
    void ScooterServiceImpl_findAll() {
        doReturn(mockedScooters).when(scooterRepository).findAll();

        List<ScooterReadDto> expected = scooterService.findAll();
        List<ScooterReadDto> actual = mockedScooterReadDtos;

        assertEquals(expected, actual);
    }

    @Test
    void ScooterServiceImpl_changeStatus() {
        doReturn(true).when(scooterRepository).changeStatus(id, scooterCreateDto.getScooterState());

        boolean changeStatus = scooterService.changeStatus(id, scooterCreateDto);

        assertTrue(changeStatus);
    }

    @Test
    void ScooterServiceImpl_changeCustomerId() {
        doReturn(true).when(scooterRepository).changeCustomerId(id, id);

        boolean changeCustomerId = scooterService.changeCustomerId(id, id);

        assertTrue(changeCustomerId);
    }

    @Test
    void ScooterServiceImpl_findById() {
        doReturn(scooter).when(scooterRepository).findById(id);

        ScooterReadDto expected = scooterService.findById(id);
        ScooterReadDto actual = scooterReadDto;

        assertEquals(expected, actual);
    }

    @Test
    void ScooterServiceImpl_update() {
        doReturn(true).when(scooterRepository).update(id, scooterCreateDto.getNumber(),
                scooterCreateDto.getCharacteristicId(), scooterCreateDto.getScooterState(),
                scooterCreateDto.getRentalPointId());

        boolean update = scooterService.update(id, scooterCreateDto);

        assertTrue(update);
    }

    @Test
    void ScooterServiceImpl_delete() {
        doReturn(null).when(scooterRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> scooterService.delete(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void ScooterServiceImpl_create() {
        doReturn(scooter).when(scooterCreateMapper).map(scooterCreateDto);
        doReturn(scooter).when(scooterRepository).create(scooter);

        Scooter expected = scooterService.create(scooterCreateDto);
        Scooter actual = scooter;

        assertEquals(expected, actual);
    }

    @Test
    void ScooterServiceImpl_scootersDetails() {
        doReturn(null).when(scooterRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> scooterService.scootersDetails(id));

        assertNotNull(serviceException.getMessage());
    }
}