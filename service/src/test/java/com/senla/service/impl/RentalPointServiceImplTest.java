package com.senla.service.impl;

import com.senla.dao.repository.RentalPointRepository;
import com.senla.dto.dto.RentalPointCreateDto;
import com.senla.dto.dto.RentalPointReadDto;
import com.senla.dto.mapper.RentalPointCreateMapper;
import com.senla.dto.mapper.RentalPointReadMapper;
import com.senla.entity.RentalPoint;
import com.senla.service.RentalPointService;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class RentalPointServiceImplTest {

    @Autowired
    private RentalPointRepository rentalPointRepository;
    @Autowired
    private RentalPointService rentalPointService;
    @Autowired
    private RentalPointReadMapper rentalPointReadMapper;
    @Autowired
    private RentalPointCreateMapper rentalPointCreateMapper;
    private Long id = 1L;
    private List<RentalPoint> mockedRentalPoints = mock(List.class);
    private List<RentalPointReadDto> mockedRentalPointReadDtos = mock(List.class);
    private RentalPoint rentalPoint = mock(RentalPoint.class);
    private RentalPointReadDto rentalPointReadDto = mock(RentalPointReadDto.class);
    private RentalPointCreateDto rentalPointCreateDto = mock(RentalPointCreateDto.class);

    @BeforeEach
    void init() {
        mockedRentalPoints = List.of(new RentalPoint("Grodno", "Lenina"),
                new RentalPoint("Grodno", "Gorky"),
                new RentalPoint("Lida", "Lenina"));
        mockedRentalPointReadDtos = mockedRentalPoints.stream().map(rentalPointReadMapper::map)
                .collect(Collectors.toList());
        rentalPoint = new RentalPoint("Grodno", "Lenina");
        rentalPointReadDto = rentalPointReadMapper.map(rentalPoint);
        rentalPointCreateDto = RentalPointCreateDto.builder()
                .city("Grodno")
                .street("Lenina")
                .build();
    }

    @Test
    void RentalPointServiceImpl_findAll() {
        doReturn(mockedRentalPoints).when(rentalPointRepository).findAll();

        List<RentalPointReadDto> expected = rentalPointService.findAll();
        List<RentalPointReadDto> actual = mockedRentalPointReadDtos;

        assertEquals(expected, actual);
    }

    @Test
    void RentalPointServiceImpl_findById() {
        doReturn(rentalPoint).when(rentalPointRepository).findById(id);

        RentalPointReadDto expected = rentalPointService.findById(id);
        RentalPointReadDto actual = rentalPointReadDto;

        assertEquals(expected, actual);
    }

    @Test
    void RentalPointServiceImpl_update() {
        doReturn(true).when(rentalPointRepository).updateRentalPoint(id,
                rentalPointCreateDto.getCity(), rentalPointCreateDto.getStreet());

        boolean update = rentalPointService.update(id, rentalPointCreateDto);

        assertTrue(update);
    }

    @Test
    void RentalPointServiceImpl_delete() {
        doReturn(null).when(rentalPointRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> rentalPointService.delete(id));

        assertNotNull(serviceException.getMessage());
    }

    @Test
    void RentalPointServiceImpl_create() {
        doReturn(rentalPoint).when(rentalPointCreateMapper).map(rentalPointCreateDto);
        doReturn(rentalPoint).when(rentalPointRepository).create(rentalPoint);

        RentalPoint expected = rentalPointService.create(rentalPointCreateDto);
        RentalPoint actual = rentalPoint;

        assertEquals(expected, actual);
    }

    @Test
    void RentalPointServiceImpl_showScooters() {
        doReturn(null).when(rentalPointRepository).findById(id);

        ServiceException serviceException = assertThrows(ServiceException.class,
                () -> rentalPointService.showScooters(id));

        assertNotNull(serviceException.getMessage());
    }
}