package com.senla.service.impl;

import com.senla.dao.repository.RoleRepository;
import com.senla.dao.repository.UserRepository;
import com.senla.dto.dto.UserCreateDto;
import com.senla.dto.mapper.UserCreateMapper;
import com.senla.entity.Role;
import com.senla.entity.User;
import com.senla.service.config.TestConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestConfig.class, loader = AnnotationConfigContextLoader.class)
class UserServiceTest {

    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserCreateMapper userCreateMapper;
    private Role role = mock(Role.class);
    private User user = mock(User.class);
    private UserCreateDto userCreateDto = mock(UserCreateDto.class);
    private String login = "User";

    @BeforeEach
    void init() {
        role = new Role("USER");
        user = new User("Petr", "Petrov");
        userCreateDto = UserCreateDto.builder()
                .login("petr")
                .password("123456678")
                .build();
    }

    @Test
    void UserService_create() {
        doReturn(role).when(roleRepository).findByName(login);
        doReturn(user).when(userCreateMapper).map(userCreateDto);
        doReturn(user).when(userRepository).create(user);

        User expected = userService.create(userCreateDto);
        User actual = user;

        assertEquals(expected, actual);
    }

    @Test
    void UserService_findByLogin() {
        doReturn(user).when(userRepository).findByLogin(login);

        User expected = userService.findByLogin(login);
        User actual = user;

        assertEquals(expected, actual);
    }
}