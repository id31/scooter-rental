package com.senla.dto.mapper;

import com.senla.dto.dto.RentalPointReadDto;
import com.senla.entity.RentalPoint;
import org.springframework.stereotype.Component;

@Component
public class RentalPointReadMapper implements Mapper<RentalPoint, RentalPointReadDto> {

    @Override
    public RentalPointReadDto map(RentalPoint object) {
        return RentalPointReadDto.builder()
                .city(object.getCity())
                .street(object.getStreet())
                .build();
    }
}
