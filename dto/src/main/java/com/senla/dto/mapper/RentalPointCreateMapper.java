package com.senla.dto.mapper;

import com.senla.dto.dto.RentalPointCreateDto;
import com.senla.entity.RentalPoint;
import org.springframework.stereotype.Component;

@Component
public class RentalPointCreateMapper implements Mapper<RentalPointCreateDto, RentalPoint> {

    @Override
    public RentalPoint map(RentalPointCreateDto object) {
        RentalPoint rentalPoint = new RentalPoint();
        rentalPoint.setCity(object.getCity());
        rentalPoint.setStreet(object.getStreet());
        return rentalPoint;
    }
}
