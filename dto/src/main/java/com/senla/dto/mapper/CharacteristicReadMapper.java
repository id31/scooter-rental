package com.senla.dto.mapper;

import com.senla.dto.dto.CharacteristicReadDto;
import com.senla.entity.Characteristic;
import org.springframework.stereotype.Component;

@Component
public class CharacteristicReadMapper implements Mapper<Characteristic, CharacteristicReadDto> {

    @Override
    public CharacteristicReadDto map(Characteristic object) {
        return CharacteristicReadDto.builder()
                .brand(object.getBrand())
                .model(object.getModel())
                .speed(object.getSpeed())
                .power(object.getPower())
                .build();
    }
}

