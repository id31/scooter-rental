package com.senla.dto.mapper;

import com.senla.dto.dto.TariffCreateDto;
import com.senla.entity.Tariff;
import org.springframework.stereotype.Component;

@Component
public class TariffCreateMapper implements Mapper<TariffCreateDto, Tariff> {

    @Override
    public Tariff map(TariffCreateDto object) {
        Tariff tariff = new Tariff();
        tariff.setTariffType(object.getTariffType());
        tariff.setPrice(object.getPrice());
        return tariff;
    }
}
