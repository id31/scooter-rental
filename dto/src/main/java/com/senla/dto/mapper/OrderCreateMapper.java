package com.senla.dto.mapper;

import com.senla.dao.repository.CustomerRepository;
import com.senla.dao.repository.ScooterRepository;
import com.senla.dao.repository.TariffRepository;
import com.senla.dto.dto.OrderCreateDto;
import com.senla.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderCreateMapper implements Mapper<OrderCreateDto, Order> {

    @Autowired
    private ScooterRepository scooterRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private TariffRepository tariffRepository;

    @Override
    public Order map(OrderCreateDto object) {
        Order order = new Order();
        order.setDateOrder(object.getDateOrder());
        order.setDateRefund(object.getDateRefund());
        order.setMileage(object.getMileage());
        order.setQuantityHours(object.getQuantityHours());
        order.setScooter(scooterRepository.findById(object.getScooterId()));
        order.setCustomer(customerRepository.findById(object.getCustomerId()));
        order.setTariff(tariffRepository.findById(object.getTariffId()));
        return order;
    }
}
