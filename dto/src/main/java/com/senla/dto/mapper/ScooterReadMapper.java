package com.senla.dto.mapper;

import com.senla.dto.dto.CharacteristicReadDto;
import com.senla.dto.dto.ScooterReadDto;
import com.senla.entity.Scooter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ScooterReadMapper implements Mapper<Scooter, ScooterReadDto> {

    @Autowired
    private CharacteristicReadMapper characteristicReadMapper;

    @Override
    public ScooterReadDto map(Scooter object) {
        CharacteristicReadDto characteristicReadDto = Optional.ofNullable(object.getCharacteristic())
                .map(characteristicReadMapper::map)
                .orElse(null);
        return ScooterReadDto.builder()
                .scooterState(object.getScooterState())
                .number(object.getNumber())
                .characteristicReadDto(characteristicReadDto)
                .build();
    }
}
