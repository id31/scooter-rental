package com.senla.dto.mapper;

import com.senla.dao.repository.CharacteristicRepository;
import com.senla.dao.repository.RentalPointRepository;
import com.senla.dto.dto.ScooterCreateDto;
import com.senla.entity.Scooter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ScooterCreateMapper implements Mapper<ScooterCreateDto, Scooter> {

    @Autowired
    private CharacteristicRepository characteristicRepository;
    @Autowired
    private RentalPointRepository rentalPointRepository;

    @Override
    public Scooter map(ScooterCreateDto object) {
        Scooter scooter = new Scooter();
        scooter.setScooterState(object.getScooterState());
        scooter.setCharacteristic(characteristicRepository.findById(object.getCharacteristicId()));
        scooter.setNumber(object.getNumber());
        scooter.setRentalPoint(rentalPointRepository.findById(object.getRentalPointId()));
        return scooter;
    }
}
