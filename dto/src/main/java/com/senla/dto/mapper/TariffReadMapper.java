package com.senla.dto.mapper;

import com.senla.dto.dto.TariffReadDto;
import com.senla.entity.Tariff;
import org.springframework.stereotype.Component;

@Component
public class TariffReadMapper implements Mapper<Tariff, TariffReadDto> {

    @Override
    public TariffReadDto map(Tariff object) {
        return TariffReadDto.builder()
                .tariffType(object.getTariffType())
                .price(object.getPrice())
                .build();
    }
}
