package com.senla.dto.mapper;

import com.senla.dto.dto.UserCreateDto;
import com.senla.entity.User;
import org.springframework.stereotype.Component;

@Component
public class UserCreateMapper implements Mapper<UserCreateDto, User> {

    @Override
    public User map(UserCreateDto object) {
        User user = new User();
        user.setLogin(object.getLogin());
        user.setPassword(object.getPassword());
        return user;
    }
}
