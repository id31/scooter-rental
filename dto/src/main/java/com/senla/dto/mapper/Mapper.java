package com.senla.dto.mapper;

public interface Mapper <F, T>{

    T map (F object);
}
