package com.senla.dto.mapper;

import com.senla.dto.dto.CharacteristicCreateDto;
import com.senla.entity.Characteristic;
import org.springframework.stereotype.Component;

@Component
public class CharacteristicCreateMapper implements Mapper<CharacteristicCreateDto, Characteristic> {

    @Override
    public Characteristic map(CharacteristicCreateDto object) {
        Characteristic characteristic = new Characteristic();
        characteristic.setBrand(object.getBrand());
        characteristic.setModel(object.getModel());
        characteristic.setSpeed(object.getSpeed());
        characteristic.setPower(object.getPower());
        characteristic.setWeight(object.getWeight());
        characteristic.setSizeWheels(object.getSizeWheels());
        return characteristic;
    }
}
