package com.senla.dto.mapper;

import com.senla.dto.dto.CustomerCreateDto;
import com.senla.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerCreateMapper implements Mapper<CustomerCreateDto, Customer> {

    @Override
    public Customer map(CustomerCreateDto object) {
        Customer customer = new Customer();
        customer.setName(object.getName());
        customer.setLastName(object.getLastName());
        customer.setEmail(object.getEmail());
        customer.setPhoneNumber(object.getPhoneNumber());
        return customer;
    }
}
