package com.senla.dto.mapper;

import com.senla.dto.dto.CustomerReadDto;
import com.senla.entity.Customer;
import org.springframework.stereotype.Component;

@Component
public class CustomerReadMapper implements Mapper<Customer, CustomerReadDto> {

    @Override
    public CustomerReadDto map(Customer object) {
        return CustomerReadDto.builder()
                .name(object.getName())
                .lastName(object.getLastName())
                .email(object.getEmail())
                .phoneNumber(object.getPhoneNumber())
                .build();
    }
}
