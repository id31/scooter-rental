package com.senla.dto.mapper;

import com.senla.dto.dto.OrderReadDto;
import com.senla.dto.dto.TariffReadDto;
import com.senla.entity.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class OrderReadMapper implements Mapper<Order, OrderReadDto> {

    @Autowired
    private TariffReadMapper tariffReadMapper;

    @Override
    public OrderReadDto map(Order object) {
        TariffReadDto tariffReadDto = Optional.ofNullable(object.getTariff())
                .map(tariffReadMapper::map)
                .orElse(null);
        return OrderReadDto.builder()
                .dateOrder(object.getDateOrder())
                .dateRefund(object.getDateRefund())
                .mileage(object.getMileage())
                .tariffReadDto(tariffReadDto)
                .quantityHours(object.getQuantityHours())
                .build();
    }
}
