package com.senla.dto.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class RentalPointReadDto {

    String city;
    String street;
}
