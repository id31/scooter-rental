package com.senla.dto.dto;

import com.senla.entity.TariffType;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Builder
@Data
public class TariffReadDto {

    TariffType tariffType;
    BigDecimal price;
}
