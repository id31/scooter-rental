package com.senla.dto.dto;

import com.senla.dto.validator.EnumNamePattern;
import com.senla.entity.TariffType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class TariffCreateDto {

    @NotNull(message = "Tariff type can't be blank")
    @EnumNamePattern(regexp = "(HOURLY|SUBSCRIPTION)")
    TariffType tariffType;
    @NotNull(message = "Price can't be blank")
    BigDecimal price;
}
