package com.senla.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class RentalPointCreateDto {

    @NotBlank(message = "City can't be blank")
    String city;
    @NotBlank(message = "Street can't be blank")
    String street;
}
