package com.senla.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CustomerCreateDto {

    @NotBlank(message = "Name can't be blank")
    String name;
    @NotBlank(message = "Last name can't be blank")
    String lastName;
    @NotBlank(message = "Email can't be blank")
    @Email(message = "Incorrect email")
    String email;
    @NotBlank(message = "Phone number can't be blank")
    @Pattern(regexp = "\\+375(33|29|44|25)[0-9]{7}", message = "Incorrect phone number")
    String phoneNumber;
}
