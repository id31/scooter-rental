package com.senla.dto.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CharacteristicReadDto {

    String brand;
    String model;
    Long speed;
    Long power;
}
