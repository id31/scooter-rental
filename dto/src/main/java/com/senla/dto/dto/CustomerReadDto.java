package com.senla.dto.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CustomerReadDto {

    String name;
    String lastName;
    String phoneNumber;
    String email;
}
