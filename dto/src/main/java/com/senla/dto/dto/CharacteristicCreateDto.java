package com.senla.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class CharacteristicCreateDto {

    @NotBlank(message = "Brand can't be blank")
    @Pattern(regexp = "(Yedoo|Go-Ped|Micro|SHULZ|Globber)")
    String brand;
    @NotBlank(message = "Model can't  be blank")
    String model;
    @NotNull(message = "Speed can't be blank")
    Long speed;
    @NotNull(message = "Power can't be blank")
    Long power;
    @NotNull(message = "Weight can't be blank")
    Integer weight;
    @NotNull(message = "Size wheels can't be blank")
    Integer sizeWheels;
}
