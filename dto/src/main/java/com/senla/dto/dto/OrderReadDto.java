package com.senla.dto.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Builder
@Data
public class OrderReadDto {

    LocalDate dateOrder;
    LocalDate dateRefund;
    Long mileage;
    TariffReadDto tariffReadDto;
    Integer quantityHours;
}
