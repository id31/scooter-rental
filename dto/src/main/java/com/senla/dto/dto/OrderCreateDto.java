package com.senla.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class OrderCreateDto {

    @NotNull(message = "Date order can't be empty")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate dateOrder;
    @NotNull(message = "Date refund can't be empty")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    LocalDate dateRefund;
    @NotNull(message = "Mileage can't be null")
    Long mileage;
    @NotNull(message = "Tariff's ID can't be null")
    Long tariffId;
    @NotNull(message = "Quantity minute can't be null")
    @Min(value = 1, message = "Value can be 1 or more")
    Integer quantityHours;
    @NotNull(message = "Customer's ID can't be null")
    Long customerId;
    @NotNull(message = "Scooter's ID can't be null")
    Long scooterId;
}
