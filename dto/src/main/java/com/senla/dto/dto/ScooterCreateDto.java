package com.senla.dto.dto;

import com.senla.dto.validator.EnumNamePattern;
import com.senla.entity.ScooterState;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class ScooterCreateDto {

    @NotNull(message = "Number can't be null")
    Integer number;
    @NotNull(message = "Characteristic ID can't be null")
    Long characteristicId;
    @NotNull(message = "Scooter's state can't be null")
    @EnumNamePattern(regexp = "(FREE|USED|REPAIRED)", message = "Incorrect scooter state")
    ScooterState scooterState;
    @NotNull(message = "Rental point ID can't be null")
    Long rentalPointId;
}
