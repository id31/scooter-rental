package com.senla.dto.dto;

import lombok.Data;

@Data
public class UserReadDto {

    String login;
    String password;
}
