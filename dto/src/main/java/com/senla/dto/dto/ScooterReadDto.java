package com.senla.dto.dto;

import com.senla.entity.ScooterState;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ScooterReadDto {

    Integer number;
    ScooterState scooterState;
    CharacteristicReadDto characteristicReadDto;
}
