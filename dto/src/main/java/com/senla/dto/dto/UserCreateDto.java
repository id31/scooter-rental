package com.senla.dto.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class UserCreateDto {

    @NotEmpty(message = "The login can't be blank")
    String login;
    @NotEmpty(message = "Password can't be blank")
    @Size(min = 8, message = "Password must contain at least 8 characteristics")
    String password;
}
