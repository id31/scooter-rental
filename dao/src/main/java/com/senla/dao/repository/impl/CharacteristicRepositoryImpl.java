package com.senla.dao.repository.impl;

import com.senla.dao.dao.AbstractDao;
import com.senla.dao.dao.DaoException;
import com.senla.dao.repository.CharacteristicRepository;
import com.senla.entity.Characteristic;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Slf4j
@Repository
public class CharacteristicRepositoryImpl extends AbstractDao<Long, Characteristic>
        implements CharacteristicRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    public CharacteristicRepositoryImpl() {
        setClazz(Characteristic.class);
    }

    @Override
    public boolean updateCharacteristic(Long id, String brand, String model, Long speed,
                                        Long power, Integer weight, Integer sizeWheels) {
        try {
            return entityManager.createNativeQuery("update characteristics " +
                                    "set brand=:brand, model=:model, speed=:speed, " +
                                    "power=:power, weight=:weight, size_wheels=:sizeWheels " +
                                    "where id=:id",
                            Characteristic.class)
                    .setParameter("brand", brand)
                    .setParameter("model", model)
                    .setParameter("speed", speed)
                    .setParameter("power", power)
                    .setParameter("weight", weight)
                    .setParameter("sizeWheels", sizeWheels)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to update characteristic", e);
            throw new DaoException(e);
        }
    }
}
