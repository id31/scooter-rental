package com.senla.dao.repository;

import com.senla.dao.dao.GenericDao;
import com.senla.entity.Scooter;
import com.senla.entity.ScooterState;

import java.util.List;

public interface ScooterRepository extends GenericDao<Long, Scooter> {

    boolean update(Long id, Integer number, Long characteristicId, ScooterState scooterState,
                   Long rentalPointId);

    boolean changeStatus(Long id, ScooterState scooterState);

    boolean changeCustomerId(Long id, Long customerId);

    List detailsScooter(Long id);
}
