package com.senla.dao.repository;

import com.senla.dao.dao.GenericDao;
import com.senla.entity.Customer;

import java.util.List;

public interface CustomerRepository extends GenericDao<Long, Customer> {
    boolean updateCustomer(Long id, String name, String lastName, String phoneNumber, String email);

    List customersHistory(Long id);
}
