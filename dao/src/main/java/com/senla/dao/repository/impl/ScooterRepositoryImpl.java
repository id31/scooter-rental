package com.senla.dao.repository.impl;

import com.senla.dao.dao.AbstractDao;
import com.senla.dao.dao.DaoException;
import com.senla.dao.repository.ScooterRepository;
import com.senla.entity.Scooter;
import com.senla.entity.ScooterState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Repository
public class ScooterRepositoryImpl extends AbstractDao<Long, Scooter> implements ScooterRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    public ScooterRepositoryImpl() {
        setClazz(Scooter.class);
    }

    @Override
    public boolean update(Long id, Integer number, Long characteristicId,
                          ScooterState scooterState, Long rentalPointId) {
        try {
            return entityManager.createNativeQuery("update scooters " +
                            "set number=:number, characteristic_id=:characteristicId, " +
                            "scooter_state=:scooterState, rental_point_id=:rentalPointId " +
                            "where id=:id", Scooter.class)
                    .setParameter("number", number)
                    .setParameter("characteristicId", characteristicId)
                    .setParameter("scooterState", String.valueOf(scooterState))
                    .setParameter("rentalPointId", rentalPointId)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to update scooter");
            throw new DaoException(e);
        }
    }

    @Override
    public boolean changeStatus(Long id, ScooterState scooterState) {
        try {
            return entityManager.createNativeQuery("update scooters " +
                            "set  scooter_state=:status " +
                            "where id=:id", Scooter.class)
                    .setParameter("id", id)
                    .setParameter("status", String.valueOf(scooterState))
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to change status", e);
            throw new DaoException(e);
        }
    }

    @Override
    public boolean changeCustomerId(Long id, Long customerId) {
        try {
            return entityManager.createNativeQuery("update scooters " +
                            "set customer_id=:customerId " +
                            "where id=:id", Scooter.class)
                    .setParameter("customerId", customerId)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to change customer Id", e);
            throw new DaoException(e);
        }
    }

    @Override
    public List detailsScooter(Long id) {
        try {
            return entityManager.createNativeQuery("select c.name, c.last_name, " +
                            "o.date_order, o.date_refund, o.mileage " +
                            "from scooters s " +
                            "join orders o on s.id = o.scooter_id " +
                            "join customers c on c.id = o.customer_id " +
                            "where o.scooter_id=:id")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            log.error("Exception while trying to get scooter's details", e);
            throw new DaoException(e);
        }
    }
}
