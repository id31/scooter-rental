package com.senla.dao.repository;

import com.senla.entity.User;

public interface UserRepository {

    User findByLogin(String login);

    User create(User user);
}
