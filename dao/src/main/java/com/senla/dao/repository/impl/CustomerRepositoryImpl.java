package com.senla.dao.repository.impl;

import com.senla.dao.dao.AbstractDao;
import com.senla.dao.dao.DaoException;
import com.senla.dao.repository.CustomerRepository;
import com.senla.entity.Customer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Repository
public class CustomerRepositoryImpl extends AbstractDao<Long, Customer> implements CustomerRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    public CustomerRepositoryImpl() {
        setClazz(Customer.class);
    }

    @Override
    public boolean updateCustomer(Long id, String name, String lastName,
                                  String phoneNumber, String email) {
        try {
            return entityManager.createNativeQuery("update Customers " +
                            "set name=:name, last_name=:lastName, " +
                            "phone_number=:phoneNumber, email=:email " +
                            "where id=:id", Customer.class)
                    .setParameter("name", name)
                    .setParameter("lastName", lastName)
                    .setParameter("phoneNumber", phoneNumber)
                    .setParameter("email", email)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to update customer", e);
            throw new DaoException(e);
        }
    }

    @Override
    public List customersHistory(Long id) {
        try {
            return entityManager.createNativeQuery("select o.date_order, o.date_refund, " +
                            "o.mileage, s.number" +
                            " from orders o " +
                            "join scooters s on s.id = o.scooter_id " +
                            "where o.customer_id=:id")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            log.error("Exception while trying to get customer's history", e);
            throw new DaoException(e);
        }
    }
}
