package com.senla.dao.repository;

import com.senla.entity.Role;

public interface RoleRepository {

    Role findByName(String login);
}
