package com.senla.dao.repository.impl;

import com.senla.dao.dao.AbstractDao;
import com.senla.dao.dao.DaoException;
import com.senla.dao.repository.RentalPointRepository;
import com.senla.entity.RentalPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Slf4j
@Repository
public class RentalPointRepositoryImpl extends AbstractDao<Long, RentalPoint>
        implements RentalPointRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    public RentalPointRepositoryImpl() {
        setClazz(RentalPoint.class);
    }

    @Override
    public boolean updateRentalPoint(Long id, String city, String street) {
        try {
            return entityManager.createNativeQuery("update rental_points " +
                                    "set city=:city, street=:street " +
                                    "where id=:id",
                            RentalPoint.class)
                    .setParameter("city", city)
                    .setParameter("street", street)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to update rental point", e);
            throw new DaoException(e);
        }
    }

    @Override
    public List showRentalPointScooters(Long id) {
        try {
            return entityManager.createNativeQuery("select r.id, s.scooter_state, c.model, COUNT(c.model)  " +
                            "from rental_points r " +
                            "join scooters s on r.id = s.rental_point_id " +
                            "join characteristics c on c.id = s.characteristic_id " +
                            "where r.id=:id " +
                            "group by s.scooter_state, r.id, c.model")
                    .setParameter("id", id)
                    .getResultList();
        } catch (Exception e) {
            log.error("Exception while trying to get list scooters", e);
            throw new DaoException(e);
        }
    }
}
