package com.senla.dao.repository;

import com.senla.dao.dao.GenericDao;
import com.senla.entity.Characteristic;

public interface CharacteristicRepository extends GenericDao<Long, Characteristic> {
    boolean updateCharacteristic(Long id, String brand, String model, Long speed, Long power, Integer weight,
                                 Integer sizeWheels);
}
