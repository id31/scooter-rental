package com.senla.dao.repository.impl;

import com.senla.dao.dao.AbstractDao;
import com.senla.dao.dao.DaoException;
import com.senla.dao.repository.OrderRepository;
import com.senla.entity.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;

@Slf4j
@Repository
public class OrderRepositoryImpl extends AbstractDao<Long, Order> implements OrderRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    public OrderRepositoryImpl() {
        setClazz(Order.class);
    }

    @Override
    public boolean updateOrder(Long id, LocalDate dateOrder, LocalDate dateRefund,
                               Long tariffId, Integer quantityHours,
                               Long customerId, Long scooterId) {
        try {
            return entityManager.createNativeQuery("update orders " +
                            "set date_order=:dateOrder, date_refund=:dateRefund," +
                            "tariff_id=:tariffId, quantity=:quantityHours, " +
                            "customer_id=:customerId, scooter_id=:scooterId " +
                            "where id=:id", Order.class)
                    .setParameter("dateOrder", dateOrder)
                    .setParameter("dateRefund", dateRefund)
                    .setParameter("tariffId", tariffId)
                    .setParameter("quantityHours", quantityHours)
                    .setParameter("customerId", customerId)
                    .setParameter("scooterId", scooterId)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to update order", e);
            throw new DaoException(e);
        }
    }
}
