package com.senla.dao.repository.impl;

import com.senla.dao.dao.AbstractDao;
import com.senla.dao.dao.DaoException;
import com.senla.dao.repository.TariffRepository;
import com.senla.entity.Tariff;
import com.senla.entity.TariffType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigDecimal;

@Slf4j
@Repository
public class TariffRepositoryImpl extends AbstractDao<Long, Tariff> implements TariffRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    public TariffRepositoryImpl() {
        setClazz(Tariff.class);
    }

    @Override
    public boolean updateTariff(Long id, TariffType tariffType, BigDecimal price) {
        try {
            return entityManager.createNativeQuery("update tariffs " +
                            "set tariff_type=:tariffType, price=:price " +
                            "where id=:id", Tariff.class)
                    .setParameter("tariffType", String.valueOf(tariffType))
                    .setParameter("price", price)
                    .setParameter("id", id)
                    .executeUpdate() > 0;
        } catch (Exception e) {
            log.error("Exception while trying to update tariff", e);
            throw new DaoException(e);
        }
    }
}
