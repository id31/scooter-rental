package com.senla.dao.repository;

import com.senla.dao.dao.GenericDao;
import com.senla.entity.Tariff;
import com.senla.entity.TariffType;

import java.math.BigDecimal;

public interface TariffRepository extends GenericDao<Long, Tariff> {
    boolean updateTariff(Long id, TariffType tariffType, BigDecimal price);
}
