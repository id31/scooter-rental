package com.senla.dao.repository.impl;

import com.senla.dao.repository.RoleRepository;
import com.senla.entity.Role;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class RoleRepositoryImpl implements RoleRepository {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public Role findByName(String login) {
        return entityManager.createQuery("select r from Role r " +
                        "where r.name = :name", Role.class)
                .setParameter("name", login)
                .getSingleResult();
    }
}
