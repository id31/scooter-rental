package com.senla.dao.repository;

import com.senla.dao.dao.GenericDao;
import com.senla.entity.RentalPoint;
import com.senla.entity.Scooter;

import java.util.List;

public interface RentalPointRepository extends GenericDao<Long, RentalPoint> {
    boolean updateRentalPoint(Long id, String city, String street);

    List<Scooter> showRentalPointScooters(Long id);
}
