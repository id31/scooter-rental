package com.senla.dao.repository.impl;

import com.senla.dao.repository.UserRepository;
import com.senla.entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class UserRepositoryImpl implements UserRepository {
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public User findByLogin(String login) {
        return (User) entityManager.createNativeQuery("select u.id, u.login, u.password " +
                        "from users u " +
                        "where login=:login", User.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public User create(User user) {
        entityManager.persist(user);
        return user;
    }
}
