package com.senla.dao.repository;

import com.senla.dao.dao.GenericDao;
import com.senla.entity.Order;

import java.time.LocalDate;

public interface OrderRepository extends GenericDao<Long, Order> {
    boolean updateOrder(Long id, LocalDate dateOrder, LocalDate dateRefund, Long tariffId, Integer quantityHours,
                        Long customerId, Long scooterId);
}
