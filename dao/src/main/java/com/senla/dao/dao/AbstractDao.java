package com.senla.dao.dao;

import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.io.Serializable;
import java.util.List;

@Slf4j
public abstract class AbstractDao<ID extends Serializable, T> implements GenericDao<ID, T> {

    @PersistenceContext
    protected EntityManager entityManager;
    private Class<T> clazz;

    public AbstractDao() {
    }

    protected void setClazz(Class<T> clazz) {
        this.clazz = clazz;
    }

    @Override
    public List<T> findAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(clazz);
        Root<T> root = query.from(clazz);
        query.select(root);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public T findById(ID id) {
        return entityManager.find(clazz, id);
    }

    @Override
    public void update(T entity) {
        entityManager.merge(entity);
        log.info("Entity is updated");
    }

    @Override
    public void delete(T entity) {
        entityManager.remove(entity);
        log.info("Entity is deleted");
    }

    @Override
    public T create(T entity) {
        entityManager.persist(entity);
        log.info("Entity is created");
        return entity;
    }
}
