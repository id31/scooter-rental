package com.senla.dao.dao;

import java.io.Serializable;
import java.util.List;

public interface GenericDao<ID extends Serializable, T> {

    List<T> findAll();

    T findById(ID id);

    void update(T entity);

    void delete(T entity);

    T create(T entity);
}
